## [1.3.1] - 2023-06-05
### Fixed
- Start license system after plugins loaded

## [1.3.0] - 2023-06-05
### Changed
- Plugin flow ^3
- New license system

## [1.2.1] - 2020-07-10
### Fixed
- Error in update request
- Error in plugin option storage
### Changed
- Library update

## [1.1.0] - 2020-04-23
### Added
- Composer 2 support
### Changed
- Library update

## [1.0.0] - 2020-01-27
### Added
- First version.